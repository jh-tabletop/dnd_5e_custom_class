# README #

A custom class generator for players to generate classes that fit their concepts. 
Uses HTML/CSS/JS and works through the browser.

### Required to run ###

A web browser.

### How to run ###

* Open index.html

### How to use ###

It consists of a point system, by which to build classes with. 25 seemed like a good number from a brief analysis of
class totals based on an arbitrary system of feat and ability scoring. (i.e. its the same as barbarian)
Naturally not all classes are equal, so some of the more powerful classes (e.g. druid) will go over 25, or some of the weaker ones will go under (e.g. fighter) if attempting to replicate.
Otherwise it seems relatively balanced (from speculation).

There are two tabs, one for class abilities (e.g. bab, saves, hd, proficiencies), and one for class features (e.g. feats).
Each component of the two tabs will have a drop down box with a point value for each option.

There is a preview menu button for a quick overview of your choices.

To generate a pdf, click generate. Remember to give it a name and a description.

### Contributing ###

* To contribute, make a branch of master, do your changes, and then create a pull request with **branch** -> **master**, I will then review it
* Feel free to fork it and use it as you will, just don't modify master without a pull request
